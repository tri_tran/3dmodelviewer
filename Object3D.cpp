#include "Object3D.h"


bool obj3::complete::SinglePartAndTextureOrder3::load(const char* objPath, const char* texPath)
{
    // Load object
    std::vector<float> vertices;
    std::vector<float> texCoords;
    std::vector<float> normals;
    std::ifstream fin(objPath);
    if (!fin)
    {
        std::cerr << "Cannot open object file " << objPath << "\n";
        return false;
    }
    std::string line;
    float f;
    while (std::getline(fin, line))
    {
        if (line.substr(0, 2) == "v ")
        {
            std::istringstream iss(line.substr(2));
            for (int i = 0; i < 3; ++i)
            {
                iss >> f;
                vertices.push_back(f);
            }
        }
        else if (line.substr(0, 2) == "vt")
        {
            std::istringstream iss(line.substr(2));
            for (int i = 0; i < 2; ++i)
            {
                iss >> f;
                texCoords.push_back(f);
            }
        }
        else if (line.substr(0, 2) == "vn")
        {
            std::istringstream iss(line.substr(2));
            for (int i = 0; i < 3; ++i)
            {
                iss >> f;
                normals.push_back(f);
            }
        }
        else if (line.substr(0, 2) == "f ")
        {
            std::istringstream iss(line.substr(2));
            std::string token;
            while (iss >> token)
            {
                Vector3i vi;
                vi.x = atoi(token.c_str()) - 1;
                vi.y = atoi(token.substr(token.find('/')+1).c_str()) - 1;
                vi.z = atoi(token.substr(token.rfind('/')+1).c_str()) - 1;
                for (int i = 0; i < 3; ++i)
                {
                    fVertices.push_back(vertices[3*vi.x + i]);
                    fNormals.push_back(normals[3*vi.z + i]);
                }
                for (int i = 0; i < 2; ++i)
                    fTexCoords.push_back(texCoords[2*vi.y + i]);
            }
        }
    }
    fin.close();

    // Load texture
    if (!texImg.load(texPath))
    {
        std::cerr << "Cannot load texture file\n";
        return false;
    }

    return true;
}

bool obj3::complete::SinglePartAndTextureOrder3::draw()const
{
    glEnable(GL_TEXTURE_2D);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    glVertexPointer(3, GL_FLOAT, 0, &fVertices[0]);
    glNormalPointer(GL_FLOAT, 0, &fNormals[0]);
    glTexCoordPointer(2, GL_FLOAT, 0, &fTexCoords[0]);
    glDrawArrays(GL_TRIANGLES, 0, fVertices.size() / 3);

    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_VERTEX_ARRAY);
    glDisable(GL_TEXTURE_2D);
    return true;
}
