## Assumptions
- Load .obj files that:
  + Contain single part (v -> vt -> vn -> f -> END)
  + Use single texture
  + Faces contain only triangles (no quads, no polygons)
  + Faces format: v/vt/vn v/vt/vn v/vt/vn (no ommit values for vertices, texCoords, and normals)
- Load .tga texture images only.
- Each model (cat, pig, tiger) contains 3 files: .obj, .tga, and .inp (lookfrom, lookat, light, material, etc.)

## Usage
- Executable: ModelViewer.exe
- Use config.ini to change model (cat, pig, tiger).
- W to zoom in, S to zoom out.
- A, D to rotate camera AROUND Y-AXIS (not around model).
- Q, E to rotate camera AROUND X-AXIS (not around model).
- C to toggle perspective view or orthogonal view.
- R to reset to initial scene.

## Code structure
- GL folder: just in case you can't find gl.h and/or glu.h. I followed this link to setup OpenGL for mingw32: https://users.cs.jmu.edu/bernstdh/web/common/help/cpp_mingw-glut-setup.php
- camera (lookfrom, lookat, fovy, ...) info is stored as global vars in main
- light info is also stored as global vars in main
- material info is stored as public vars in class Object3D
- texture image is also stored in class Object3D
- Object3D draw() method use glDrawArrays() for maximizing render speed (at expense of duplicating vertices, texCoords, normals)
- zoom() moves camera along the line that contains lookat[4] and camera[4] (I don't use glScale to "zoom" model, but actually move the camera)
- rotate: same approach. Rotate the camera, instead of glRotate the model.
- idle() use Sleep(15). **Windows specific**
