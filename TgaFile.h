#ifndef TGAFILE_H
#define TGAFILE_H

#include <cstdio>

// Source: stackoverflow.com/questions/7046270/

struct TgaFile {
    unsigned char typeCode;
    short w;
    short h;
    unsigned char bitCount;
    unsigned char* data;

    bool load(const char*);
    TgaFile() : typeCode(-1), w(0), h(0), bitCount(0), data(NULL) {}
    ~TgaFile() { delete [] data; }
};

#endif // TGAFILE_H
